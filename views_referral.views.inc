<?php
/**
 * @file
 * views_referral.views.inc
 */

/**
 * Implement hook_views_data().
 */
function views_referral_views_data() {
  $data = array();

  $data['referral']['table']['group'] = t('User Referral');

  $data['referral']['table']['join']['users'] = array(
    'left_field' => 'uid',
    'field' => 'referral_uid',
  );

  $data['referral']['uid'] = array(
    'title' => t('Invited User'),
    'help' => t('The user who was invited'),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Invited User'),
      'title' => t('Invited Users'),
      'help' => t('The user who was invited'),
    ),
  );

  $data['referral']['flag'] = array(
    'title' => t('Flag'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Flag'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['referral']['created'] = array(
    'title' => t('Created'),
    'help' => t('Timestamp when the user was registered.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['referral']['flag_timestamp'] = array(
    'title' => t('Flag updated'),
    'help' => t('Timestamp when the flag was updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}
